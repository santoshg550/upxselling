var mongoose = require('mongoose');
var ExpressBrute = require('express-brute');
var BruteStore = require('express-brute-mongoose');
var BruteForceSchema = require('express-brute-mongoose/dist/schema');

module.exports = function() {
    var bruteStore = new BruteStore(mongoose.model('bruteforce', BruteForceSchema))
    var bruteForce = new ExpressBrute(bruteStore)

    return bruteForce.prevent
}
