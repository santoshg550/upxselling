const mongoose = require('mongoose')
const Schema = mongoose.Schema

const patientSchema = new Schema({
    id: {
        type: Number,
        required: true,
        unique: true
    },
    clinicId: {
        type: String,
        required: true
    },
    firstName: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    gender: {
        type: String,
        required: true,
    },
    age: Number,
    mobNo: Number,
    email: String,
    address: {
        line_1: String,
        line_2: String,
        country: {
            type: String,
            required: true
        },
        city: {
            type: String,
            required: true
        },
        state: {
            type: String,
            required: true
        },
        pincode: Number
    },
    updatedOn: Date
})

patientSchema.pre('save', function(next) {
    this.updatedOn = new Date

    next()
})

module.exports = mongoose.model('Patient', patientSchema)
