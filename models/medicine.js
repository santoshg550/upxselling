const mongoose = require('mongoose')
const Schema = mongoose.Schema

const medicineSchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true
    },
    price: {
        type: Number,
        required: true
    },
    manufacturer: {
        type: String,
        required: true,
    },
    strength: {
        type: String,
        required: true
    },
    updatedOn: Date
})

medicineSchema.pre('save', function(next) {
    this.updatedOn = new Date

    next()
})

module.exports = mongoose.model('Medicine', medicineSchema)
