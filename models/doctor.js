const mongoose = require('mongoose')
const Schema = mongoose.Schema

const specialty = new Schema({
    specialty: String,
    type: String,
    focus: String
})

const degree = new Schema({
    degree: {
        type: String,
        required: true
    },
    fullForm: String,
    duration: Number,
    focus: String
})

const doctorSchema = new Schema({
    clinicEmail: {
        type: String,
        required: true
    },
    name: String,
    mobNo: {
        type: Number,
        required: true,
        unique: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    address: {
        line_1: String,
        line_2: String,
        country: String,
        city: String,
        state: String,
        pincode: Number
    },
    degrees: [
        {
            degree: String,
            compYear: Number,
            university: String,
            college: String
        }
    ],
    specialties: [
        {
            experience: Number,
            residency: String,
            authority: String,
            specialty: String
        }
    ],
    updatedOn: Date,
})

doctorSchema.pre('save', function(next) {
    this.updatedOn = new Date

    next()
})

module.exports = {
    Doctor: mongoose.model('Doctor', doctorSchema),
    Specialty: mongoose.model('Specialty', specialty),
    Degree: mongoose.model('Degree', degree)
}
