const mongoose = require('mongoose')
const Schema = mongoose.Schema

const visitSchema = new Schema({
    clinicId: {
        type: String,
        required: true
    },
    docId: {
        type: String,
        required: true
    },
    patId: {
        type: Number,
        required: true
    },
    patName: {
        type: String,
        required: true
    },
    docName: {
        type: String,
        required: true
    },
    reason: {
        type: String,
        required: true
    },
    bill: Number,
    paid: Number,
    visitDate: Date,
    prescription: String,
    prescImg: Buffer,
    medicines: [{ type: Schema.Types.ObjectId, ref: 'Medicine' }],
    updatedOn: Date
})

visitSchema.pre('save', function(next) {
    this.updatedOn = new Date

    next()
})

module.exports = mongoose.model('Visit', visitSchema)
