const chai = require('chai')
const chaiHttp = require('chai-http')

const expect = chai.expect

function signin(email, password, server, callback) {
    chai
        .request(server)
        .post('/signin')
        .send({
            email: email,
            password: password
        })
        .end((err, res) => {
            expect(err).to.be.null

            expect(res).to.have.cookie('sessionId')
            expect(res).to.have.status(200)

            let cookie = res.headers['set-cookie'][0]

            callback(cookie)
        })
}

module.exports = {
    signin: signin
}
