const fs = require('fs')
const chai = require('chai')
const chaiHttp = require('chai-http')

const { prep, cleanDB } = require('./prep')
const signin = require('./before_all').signin
const Visit = require('../models/visit')
const val = require('../routes/validations').report
const { removeDup } = require('../lib/util')

const expect = chai.expect

chai.use(chaiHttp)

describe('reports', () => {
    let server
    let cookie

    before((done) => {
        prep((svr) => {
            server = svr
            signin('santoshg550@gmail.com', 'Santosh@123', server, (ck) => {
                cookie = ck
                done()
            })
        })
    })

    describe('report params', () => {
        const right = [
            {
                from: 'October 13, 2014 11:13:00',
                to: 'October 14, 2014 11:13:00'
            }
        ]

        right.forEach((date) => {
            it(date + ' is right', (done) => {
                expect(val.period(date)).to.be.an('undefined')
                done()
            })
        })

        const wrongFrom = [
            {
                from: '',
                to: 'October 14, 2014 11:13:00'
            },
            {
                from: 8,
                to: 'October 14, 2014 11:13:00'
            },
            {
                from: 'abc',
                to: 'October 14, 2014 11:13:00'
            }
        ]

        wrongFrom.forEach((date) => {
            it('from ' + date.from + ' is wrong', (done) => {
                expect(val.period(date)).to.have.property('error')
                done()
            })
        })

        const wrongTo = [
            {
                from: 'October 14, 2014 11:13:00',
                to: ''
            },
            {
                from: 'October 14, 2014 11:13:00',
                to: 8
            },
            {
                from: 'October 14, 2014 11:13:00',
                to: 'abc'
            }
        ]

        wrongTo.forEach((date) => {
            it('to ' + date.to + ' is wrong', (done) => {
                expect(val.period(date)).to.have.property('error')
                done()
            })
        })

        it('from can not be greater than to', (done) => {
            expect(val.period({
                from: 'October 18, 2014 11:13:00',
                to: 'October 14, 2014 11:13:00'
            })).to.have.property('error')

            done()
        })
    })

    describe('request report', () => {
        describe('income', () => {
            it('it should not bring report with wrong range', (done) => {
                const range = {
                    from: '2018-0201T18:30:00.000Z',
                    to: new Date(2018, 01, 20)
                }

                chai
                    .request(server)
                    .post('/reports/income')
                    .set('Cookie', cookie)
                    .send(range)
                    .end(function(err, res) {
                        expect(err).to.be.null
                        expect(res).to.have.status(400)

                        done()
                    })
            })

            it('no documents in range', (done) => {
                const range = {
                    from: new Date(2014, 02),
                    to: new Date(2014, 03)
                }

                chai
                    .request(server)
                    .post('/reports/income')
                    .set('Cookie', cookie)
                    .send(range)
                    .end(function(err, res) {
                        expect(err).to.be.null
                        expect(res).to.have.status(204)

                        done()
                    })
            })

            it('it should bring report with valid range', (done) => {
                Visit.find({ visitDate: {
                    $gte: new Date(2014, 02),
                    $lte: new Date
                }}, (err, docs) => {
                    expect(err).to.be.null
                    expect(docs.length).to.gt(0)

                    const range = {
                        from: docs[0].visitDate,
                        to: docs[docs.length - 1].visitDate
                    }

                    let income = 0

                    for(let i = 0; i < docs.length; i++)
                        income += docs[i].bill

                    chai
                        .request(server)
                        .post('/reports/income')
                        .set('Cookie', cookie)
                        .send(range)
                        .end(function(err, res) {
                            expect(err).to.be.null
                            expect(res.body.length).to.gt(0)
                            expect(res.body[0].income).to.equal(income)
                            expect(res).to.have.status(200)

                            done()
                        })
                })
            })
        })

        describe('visitors', () => {
            it('it should not bring report with wrong range', (done) => {
                const range = {
                    from: '2018-0201T18:30:00.000Z',
                    to: new Date(2018, 01, 20)
                }

                chai
                    .request(server)
                    .post('/reports/visits')
                    .set('Cookie', cookie)
                    .send(range)
                    .end(function(err, res) {
                        expect(err).to.be.null
                        expect(res).to.have.status(400)

                        done()
                    })
            })

            it('no documents in range', (done) => {
                const range = {
                    from: new Date(2014, 02),
                    to: new Date(2014, 03)
                }

                chai
                    .request(server)
                    .post('/reports/visits')
                    .set('Cookie', cookie)
                    .send(range)
                    .end(function(err, res) {
                        expect(err).to.be.null
                        expect(res).to.have.status(204)

                        done()
                    })
            })

            it('it should bring report with valid range', (done) => {
                Visit.find({ visitDate: {
                    $gte: new Date(2014, 02),
                    $lte: new Date
                }}, (err, docs) => {
                    expect(err).to.be.null
                    expect(docs.length).to.gt(0)

                    const range = {
                        from: docs[0].visitDate,
                        to: docs[docs.length - 1].visitDate
                    }

                    chai
                        .request(server)
                        .post('/reports/visits')
                        .set('Cookie', cookie)
                        .send(range)
                        .end(function(err, res) {
                            expect(err).to.be.null
                            expect(res.body.length).to.gt(0)

                            expect(res.body[0].totVisitors).to.equal(docs.length)
                            expect(res).to.have.status(200)

                            done()
                        })
                })
            })
        })
    })
})
