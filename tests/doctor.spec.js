const chai = require('chai')
const chaiHttp = require('chai-http')

const { Doctor, Specialty } = require('../models/doctor')
const { validateReg, validateSpec } = require('../routes/doctors_handler')
const { isSpecValid, isDegValid } = require('../routes/validations').doctor

const signin = require('./before_all').signin
const { prep, cleanDB } = require('./prep')

const should = chai.should()
const expect = chai.expect

chai.use(chaiHttp)

describe('doctors', () => {
    let server
    let cookie

    before((done) => {
        prep((svr) => {
            server = svr
            signin('santoshg550@gmail.com', 'Santosh@123', server, (ck) => {
                cookie = ck
                done()
            })
        })
    })

    let doctor = {
        name: 'John Doe',
        email: 'xyz@gmail.com',
        mobNo: 9657681798,
        address: {
            line_1: 'abc',
            line_2: 'xyz',
            country: 'India',
            state: 'Maharashtra',
            city: 'Pune',
            pincode: 323233
        },
        degrees: [{
            degree: 'BMS',
            compYear: 2013,
            university: 'alalla',
            college: 'sfskljlfjldfj'
        }],
        specialties: [{
            specialty: 'uvw',
            experience: 8,
            residency: 'uvw',
            authority: 'alallala'
        }]
    }

    describe('validations', () => {
        ((doctor) => {
            const invalidVals = {
                name: '',
                email: '',
                mobNo: 9,
                address: {
                    line_1: '',
                    line_2: '',
                    country: '',
                    state: '',
                    city: '',
                    pincode: ''
                }
            }

            const doctorFields = Object.keys(doctor)

            for(let i = 0; i < doctorFields.length; i++)
                it(invalidVals[doctorFields[i]] + ' is wrong ' + doctorFields[i], (done) => {
                    let temp = doctor[doctorFields[i]]
                    doctor[doctorFields[i]] = invalidVals[doctorFields[i]]

                    let error = validateReg(doctor)

                    expect(error).to.have.property('error')

                    done()
                })
        })(Object.assign({}, doctor))
    })

    describe('registration', function() {
        it('it should reject the registration', (done) => {
            ((doctor) => {
                doctor.email = 'ac'

                chai
                    .request(server)
                    .post('/doctors/register')
                    .set('Cookie', cookie)
                    .send(doctor)
                    .end((err, res) => {
                        expect(err).to.be.null

                        res.should.have.status(400)

                        done()
                    })
            })(Object.assign({}, doctor))
        })

        it('it should register the doctor', (done) => {
            chai
                .request(server)
                .post('/doctors/register')
                .set('Cookie', cookie)
                .send(doctor)
                .end((err, res) => {
                    expect(err).to.be.null

                    res.should.have.status(200)

                    done()
                })
        })

        it('it should not register the doctor with duplicate mobile number', (done) => {
            ((doctor) => {
                doctor.email = 'stu@gmail.com'

                chai
                    .request(server)
                    .post('/doctors/register')
                    .set('Cookie', cookie)
                    .send(doctor)
                    .end((err, res) => {
                        expect(err).to.be.null

                        expect(res).to.have.status(409)

                        done()
                    })
            })(Object.assign({}, doctor))
        })

        it('it should not register the doctor with duplicate email id', (done) => {
            ((doctor) => {
                doctor.mobNo = 9404609084

                chai
                    .request(server)
                    .post('/doctors/register')
                    .set('Cookie', cookie)
                    .send(doctor)
                    .end((err, res) => {
                        expect(err).to.be.null

                        expect(res).to.have.status(409)

                        done()
                    })
            })(Object.assign({}, doctor))
        })

        it('it should fetch the clinic doctors', (done) => {
            chai
                .request(server)
                .get('/doctors')
                .set('Cookie', cookie)
                .end((err, res) => {
                    expect(err).to.be.null

                    res.should.have.status(200)

                    done()
                })
        })

        it('update should fail', (done) => {
            Doctor.findOne( { email: 'xyz@gmail.com' }, (err, doc) => {
                expect(err).to.be.null

                expect(doc).to.be.not.null

                ;((doctor) => {
                    doctor._id = doc._id
                    doctor.email = 'uvwgmail.com'

                    chai
                        .request(server)
                        .post('/doctors/update')
                        .set('Cookie', cookie)
                        .send(doctor)
                        .end((err, res) => {
                            expect(err).to.be.null

                            res.should.have.status(400)

                            done()
                        })
                })(doctor)
            })
        })

        it('it should update the doctor', (done) => {
            Doctor.findOne({ email: 'xyz@gmail.com' }, (err, doc) => {
                expect(err).to.be.null

                expect(doc).to.be.not.null

                ;((doctor) => {
                    doctor._id = doc._id
                    doctor.email = 'uvw@gmail.com'

                    chai
                        .request(server)
                        .post('/doctors/update')
                        .set('Cookie', cookie)
                        .send(doctor)
                        .end((err, res) => {
                            expect(err).to.be.null

                            res.should.have.status(200)

                            done()
                        })
                })(doctor)
            })
        })
    })

    describe('specialty', () => {
        const specialty = {
            specialty: 'abc',
            experience: 8,
            residency: 'uvw',
            authority: 'alallala'
        }

        ;((specialty) => {
            const specFields = Object.keys(specialty)

            for(let i = 0; i < specFields.length; i++) {
                let temp = specialty[specFields[i]]
                specialty[specFields[i]] = ''

                it(specFields[i] + ' is wrong', (done) => {
                    expect(isSpecValid([specialty])).to.have.property('error')

                    specialty[specFields[i]] = temp

                    done()
                })
            }
        })(Object.assign({}, specialty))

        it('it should add specialty', (done) => {
            Doctor.findOne({ email: 'uvw@gmail.com' }, (err, doc) => {
                expect(err).to.be.null

                expect(doctor).to.be.not.null

                doctor = doc

                doctor.specialties.push(specialty)

                chai
                    .request(server)
                    .post('/doctors/update')
                    .set('Cookie', cookie)
                    .send(doctor)
                    .end((err, res) => {
                        expect(err).to.be.null

                        expect(res).to.have.status(200)

                        done()
                    })
            })
        })

        it('it should remove duplicate specialty', (done) => {
            doctor.specialties.push(specialty)

            chai
                .request(server)
                .post('/doctors/update')
                .set('Cookie', cookie)
                .send(doctor)
                .end((err, res) => {
                    expect(err).to.be.null

                    expect(res).to.have.status(200)

                    done()
                })
        })

        it('it should reject the specialty', (done) => {
            specialty.specialty = ''

            doctor.specialties = [specialty]

            chai
                .request(server)
                .post('/doctors/update')
                .set('Cookie', cookie)
                .send(doctor)
                .end((err, res) => {
                    expect(err).to.be.null

                    res.should.have.status(400)

                    done()
                })
        })
    })

    describe('degrees', function() {
        const degree = {
            degree: 'MBBS',
            compYear: 2013,
            university: 'alalla',
            college: 'sfskljlfjldfj'
        }

        ;((degree) => {
            const degFields = Object.keys(degree)

            for(let i = 0; i < degFields.length; i++) {
                let temp = degree[degFields[i]]
                degree[degFields[i]] = ''

                it(degFields[i] + ' is wrong', (done) => {
                    expect(isDegValid([degree])).to.have.property('error')

                    degree[degFields[i]] = temp

                    done()
                })
            }
        })(Object.assign({}, degree))

        it('it should add degree', (done) => {
            Doctor.findOne({ email: 'uvw@gmail.com' }, (err, doc) => {
                expect(err).to.be.null

                expect(doctor).to.be.not.null

                doctor = doc

                doctor.degrees.push(degree)

                chai
                    .request(server)
                    .post('/doctors/update')
                    .set('Cookie', cookie)
                    .send(doctor)
                    .end((err, res) => {
                        expect(err).to.be.null

                        expect(res).to.have.status(200)

                        done()
                    })
            })
        })

        it('it should remove duplicate degree', (done) => {
            doctor.degrees.push(degree)

            chai
                .request(server)
                .post('/doctors/update')
                .set('Cookie', cookie)
                .send(doctor)
                .end((err, res) => {
                    expect(err).to.be.null

                    expect(res).to.have.status(200)

                    done()
                })
        })

        it('it should reject the degree', (done) => {
            degree.degree = ''

            doctor.degrees = [degree]

            chai
                .request(server)
                .post('/doctors/update')
                .set('Cookie', cookie)
                .send(doctor)
                .end((err, res) => {
                    expect(err).to.be.null

                    res.should.have.status(400)

                    done()
                })
        })
    })
})
