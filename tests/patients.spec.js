const chai = require('chai')
const chaiHttp = require('chai-http')

const Patient = require('../models/patient')
const { prep, cleanDB } = require('./prep')
const val = require('../routes/validations').patient
const signin = require('./before_all').signin

const should = chai.should()
const expect = chai.expect

chai.use(chaiHttp)

describe('patients', function() {
    let server
    let cookie

    before((done) => {
        prep((svr) => {
            server = svr
            signin('santoshg550@gmail.com', 'Santosh@123', server, (ck) => {
                cookie = ck
                done()
            })
        })
    })

    describe('search', () => {
        const right = [ 'abc', ' uvw ', 'uvw xyz']

        it(right[0] + ' is right search query', (done) => {
            expect(val.isQueryValid(right[0])).to.equal(true)
            done()
        })

        const wrong = [ '', 8, '       ']

        wrong.forEach(function(query) {
            it(query + ' is wrong search query', (done) => {
                expect(val.isQueryValid(query)).to.equal(false)
                done()
            })
        })
    })

    describe('address', function(done) {
        describe('country state city', function(done) {
            let right = [ 'abc' ]

            for(let i = 0; i < right.length; i++) {
                let add = {
                    country: right[i],
                    city: right[i],
                    state: right[i]
                }

                it('country: ' + right[i] + ' city: ' +  right[i] + ' state: ' + right[i] + ' is right address', function(done) {
                    val.isAddressValid(add).should.equal(true)
                    done()
                })
            }

            let wrong = [ '', 5 ]

            const csc = [ 'country', 'state', 'city' ]

            for(let i = 0; i < wrong.length; i++) {
                for(let j = 0; j < csc.length; j++) {
                    let add = {
                        country: right[1],
                        city: right[1],
                        state: right[1],
                    }

                    add[csc[j]] = wrong[i]

                    it(add[csc[j]] + ' is wrong address', function(done) {
                        val.isAddressValid(add[csc[j]]).should.equal(false)
                        done()
                    })
                }
            }
        })

        describe('address optional fields', function() {
            let valAddress = {
                country: 'India',
                city: 'Pune',
                state: 'Maharashtra'
            }

            ;((valAddress) => {
                describe('pincode', function(done) {
                    /*
                     * If assignments to valAddress.pincode are done outside it,
                     * tests show wrong results because by the time tests are run,
                     * valAddress.pincode holds only the last value assigned, not
                     * the individual.
                     */
                    it(8 + ' is right pincode', function(done) {
                        valAddress.pincode = 8
                        val.isAddressValid(valAddress).should.equal(true)
                        done()
                    })

                    let wrong = [ '', 'abc' ]

                    wrong.forEach(function(pincode) {
                        it(pincode + ' is wrong pincode', function(done) {
                            valAddress.pincode = pincode
                            val.isAddressValid(valAddress).should.equal(false)
                            done()
                        })
                    })
                })
            })(Object.assign({}, valAddress))

            ;((valAddress) => {
                describe('line_2', function(done) {
                    it('abc is right line_2', function(done) {
                        valAddress.line_2 = 'abc'
                        val.isAddressValid(valAddress).should.equal(true)
                        done()
                    })

                    let wrong = [ '', 5 ]

                    wrong.forEach(function(line_2) {
                        it(line_2 + ' is wrong line_2', function(done) {
                            valAddress.line_2 = line_2
                            val.isAddressValid(valAddress).should.equal(false)
                            done()
                        })
                    })
                })
            })(Object.assign({}, valAddress))

            ;((valAddress) => {
                describe('line_1', function(done) {
                    it('abc is right line_1', function(done) {
                        valAddress.line_1 = 'abc'
                        val.isAddressValid(valAddress).should.equal(true)
                        done()
                    })

                    let wrong = [ '', 5 ]

                    wrong.forEach(function(line_1) {
                        it(line_1 + ' is wrong line_1', function(done) {
                            valAddress.line_1 = line_1
                            val.isAddressValid(valAddress).should.equal(false)
                            done()
                        })
                    })
                })
            })(Object.assign({}, valAddress))
        })
    })

    describe('gender', function(done) {
        let right = [ 'M', 'F', 'T' ]

        right.forEach(function(gender) {
            it(gender + ' is right gender', function(done) {
                val.isGenderValid(gender).should.equal(true)
                done()
            })
        })

        let wrong = [ '', 'acb', 5 ]

        wrong.forEach(function(gender) {
            it(gender + ' is wrong gender', function(done) {
                val.isGenderValid(gender).should.equal(false)
                done()
            })
        })
    })

    describe('first name', function(done) {
        let names = [ '', 4 ]

        names.forEach(function(name) {
            it('name is ' + name, function(done) {
                expect(val.isFNValid(name)).to.equal(false)
                done()
            })
        })
    })

    describe('last name', function(done) {
        let names = [ '', 4 ]

        names.forEach(function(name) {
            it('name is ' + name, function(done) {
                expect(val.isLNValid(name)).to.equal(false)
                done()
            })
        })
    })

    describe('emails', function(done) {
        let emails = [ 43, 'abc', 'bandichoderb@gmail.com' ]

        it('email is ' + emails[2], function(done) {
            val.isEmailValid(emails[2]).should.equal(true)
            done()
        })

        emails.splice(0, 2).forEach(function(email) {
            it('email is ' + email, function(done) {
                val.isEmailValid(email).should.equal(false)
                done()
            })
        })
    })

    describe('age', function(done) {
        let ages = [ '', 'abc' ]

        it('age is ' + ages[0], function(done) {
            val.isAgeValid(ages[0]).should.equal(true)
            done()
        })

        it('age is ' + ages[1], function(done) {
            val.isAgeValid(ages[1]).should.equal(false)
            done()
        })
    })

    describe('mobile numbers', function(done) {
        let mobs = [ '', 'abc', 23, 96576817988, 9657681798 ]

        it('mob is ' + mobs[0], function(done) {
            val.isMobValid(mobs[0]).should.equal(true)
            done()
        })

        it('mob is ' + mobs[4], function(done) {
            val.isMobValid(mobs[3]).should.equal(true)
            done()
        })

        mobs.splice(1, 3).forEach(function(mob) {
            it('mob is ' + mob, function(done) {
                val.isMobValid(mob).should.equal(false)
                done()
            })
        })
    })

    let patient = {
        firstName: 'Jane',
        lastName: 'Doe',
        email: 'bandichoderb@gmail.com',
        gender: 'F',
        age: 44,
        mobNo: 9657681798,
        address: {
            line_1: 'Ganganagar',
            line_2: 'Phursungi',
            country: 'India',
            city: 'Pune',
            state: 'Maharashtra',
            pincode: 412308
        }
    }

    describe('register patient', function() {
        (function(patient) {
            let invalidVal = {
                firstName: '',
                lastName: '',
                email: 'bandichoderb@.com',
                gender: '',
                age: 'abc',
                mobNo: 97681798,
                address: {
                    line_1: 89,
                    line_2: 90,
                    country: '',
                    city: '',
                    state: '',
                    pincode: 'abc'
                }
            }

            const patientFields = Object.keys(patient)

            for(let i = 0; i < patientFields.length; i++)
                it(invalidVal[patientFields[i]] + ' is wrong ' + patientFields[i], function(done) {
                    let temp = patient[patientFields[i]]
                    patient[patientFields[i]] = invalidVal[patientFields[i]]

                    chai
                        .request(server)
                        .post('/patients/register')
                        .set('Cookie', cookie)
                        .send(patient)
                        .end(function(err, res) {
                            expect(err).to.be.null
                            expect(res).to.have.status(400)

                            patient[patientFields[i]] = temp

                            done()
                        })
                })
        })(Object.assign({}, patient))

        it('it should register patient', function(done) {
            chai
                .request(server)
                .post('/patients/register')
                .set('Cookie', cookie)
                .send(patient)
                .end(function(err, res) {
                    expect(err).to.be.null
                    expect(res).to.have.status(200)

                    done()
                })
        })
    })

    describe('view patient', function() {
        it('all patients', function(done) {
            chai
                .request(server)
                .post('/patients')
                .set('Cookie', cookie)
                .end(function(err, res) {
                    expect(err).to.be.null

                    expect(res).to.be.json
                    expect(res.body).to.be.an('array')
                    expect(res).to.have.status(200)

                    done()
                })
        })

        describe('range of patients', function() {
            it('it should not fetch patient list with wrong range', function(done) {
                const range = {
                    from: 2,
                    to: 1
                }

                chai
                    .request(server)
                    .post('/patients')
                    .set('Cookie', cookie)
                    .send(range)
                    .end(function(err, res) {
                        expect(err).to.be.null

                        expect(res).to.be.json
                        expect(res).to.have.status(400)

                        done()
                    })
            })

            it('it should fetch patient list', function(done) {
                const range = {
                    from: 1,
                    to: 3
                }

                chai
                    .request(server)
                    .post('/patients')
                    .set('Cookie', cookie)
                    .send(range)
                    .end(function(err, res) {
                        expect(err).to.be.null

                        expect(res).to.be.json
                        expect(res.body.length).to.be.equal(1)
                        expect(res).to.have.status(200)

                        done()
                    })
            })

            it('from and to are same', function(done) {
                const range = {
                    from: 1,
                    to: 1
                }

                chai
                    .request(server)
                    .post('/patients')
                    .set('Cookie', cookie)
                    .send(range)
                    .end(function(err, res) {
                        expect(err).to.be.null

                        expect(res).to.be.json
                        expect(res.body.length).to.be.equal(1)
                        expect(res).to.have.status(200)


                        done()
                    })
            })

            it('it should fetch patient with given id', function(done) {
                chai
                    .request(server)
                    .post('/patients')
                    .set('Cookie', cookie)
                    .send({
                        id: 1
                    })
                    .end(function(err, res) {
                        expect(err).to.be.null

                        expect(res).to.have.status(200)

                        done()
                    })
            })
        })
    })

    describe('update patient', function() {
        patient.id = 1

        ;(function(patient) {
            let invalidVal = {
                id: '',
                firstName: '',
                lastName: '',
                email: 'bandichoderb@.com',
                gender: '',
                age: 'abc',
                mobNo: 97681798,
                address: {
                    line_1: 89,
                    line_2: 90,
                    country: '',
                    city: '',
                    state: '',
                    pincode: 'abc'
                }
            }

            const patientFields = Object.keys(patient)

            for(let i = 0; i < patientFields.length; i++)
                it(invalidVal[patientFields[i]] + ' is wrong ' + patientFields[i], function(done) {
                    let temp = patient[patientFields[i]]
                    patient[patientFields[i]] = invalidVal[patientFields[i]]

                    chai
                        .request(server)
                        .post('/patients/update')
                        .set('Cookie', cookie)
                        .send(patient)
                        .end(function(err, res) {
                            expect(err).to.be.null
                            expect(res).to.have.status(400)

                            patient[patientFields[i]] = temp

                            done()
                        })
                })
        })(Object.assign({}, patient));

        describe('it should update patient', function() {
            it('update mobNo and line_1', function(done) {
                patient.mobNo = 9404609084
                patient.address.line_1 = 'Ganganagar'

                chai
                    .request(server)
                    .post('/patients/update')
                    .set('Cookie', cookie)
                    .send(patient)
                    .end(function(err, res) {
                        expect(err).to.be.null

                        expect(res).to.have.status(200)

                        done()
                    })
            })

            it('empty age', function(done) {
                delete patient.age

                chai
                    .request(server)
                    .post('/patients/update')
                    .set('Cookie', cookie)
                    .send(patient)
                    .end(function(err, res) {
                        expect(err).to.be.null

                        expect(res).to.have.status(200)

                        done()
                    })
            })

            it('empty mobNo', function(done) {
                delete patient.mobNo

                chai
                    .request(server)
                    .post('/patients/update')
                    .set('Cookie', cookie)
                    .send(patient)
                    .end(function(err, res) {
                        expect(err).to.be.null

                        expect(res).to.have.status(200)

                        done()
                    })
            })

            it('empty email', function(done) {
                delete patient.email

                chai
                    .request(server)
                    .post('/patients/update')
                    .set('Cookie', cookie)
                    .send(patient)
                    .end(function(err, res) {
                        expect(err).to.be.null

                        expect(res).to.have.status(200)

                        done()
                    })
            })
        })
    })

    describe('search for patients', () => {
        let patients

        before((done) => {
            Patient.find((err, docs) => {
                expect(err).to.be.null

                patients = docs

                done()
            })
        })

        it('it should not fetch patient with first name', function(done) {
            chai
                .request(server)
                .post('/patients/search')
                .set('Cookie', cookie)
                .send({
                    firstName: 'abc'
                })
                .end(function(err, res) {
                    expect(err).to.be.null

                    expect(res).to.have.status(404)

                    done()
                })
        })

        it('it should not fetch patient with last name', function(done) {
            chai
                .request(server)
                .post('/patients/search')
                .set('Cookie', cookie)
                .send({
                    lastName: 'abc'
                })
                .end(function(err, res) {
                    expect(err).to.be.null

                    expect(res).to.have.status(404)

                    done()
                })
        })

        it('it should fetch patient with first name', function(done) {
            chai
                .request(server)
                .post('/patients/search')
                .set('Cookie', cookie)
                .send({
                    firstName: patients[0].firstName.slice(0, 3)
                })
                .end(function(err, res) {
                    expect(err).to.be.null

                    expect(res).to.have.status(200)
                    expect(res.body.length).to.gt(0)

                    done()
                })
        })

        it('it should fetch patient with last name', function(done) {
            chai
                .request(server)
                .post('/patients/search')
                .set('Cookie', cookie)
                .send({
                    lastName: patients[0].lastName.slice(0, 3)
                })
                .end(function(err, res) {
                    expect(err).to.be.null

                    expect(res).to.have.status(200)
                    expect(res.body.length).to.gt(0)

                    done()
                })
        })
    })
})
