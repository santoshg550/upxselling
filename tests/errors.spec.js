const chai = require('chai')
const chaiHttp = require('chai-http')

const errors = require('../lib/errors')

const expect = chai.expect

describe('errors', function() {
    describe('build error', () => {
        it('pass a string', (done) => {
            const err = errors.buildError('An error')

            expect(err).to.have.property('error')
            expect(err).to.have.property('message')

            done()
        })

        it('pass an object', (done) => {
            const err = errors.buildError({
                reason: 'Too big'
            })

            expect(err).to.have.property('error')
            expect(err).to.not.have.property('message')

            done()
        })

        it('pass an object', (done) => {
            const err = errors.buildError({
                occurredAt: 'name',
                message: 'Too short'
            })

            expect(err).to.have.property('error')
            expect(err).to.have.property('message')

            done()
        })
    })
})
