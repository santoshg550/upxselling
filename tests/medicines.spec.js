const chai = require('chai')
const chaiHttp = require('chai-http')

const Medicine = require('../models/medicine')
const { prep, cleanDB } = require('./prep')
const { validateSearch } = require('../routes/medicines_handler')
const val = require('../routes/validations').medicine
const signin = require('./before_all').signin

const expect = chai.expect

chai.use(chaiHttp)

describe('medicines', () => {
    let server
    let cookie

    before((done) => {
        prep((svr) => {
            server = svr
            signin('santoshg550@gmail.com', 'Santosh@123', server, (ck) => {
                cookie = ck
                done()
            })
        })
    })

    describe('search param', () => {
        const right = [ 'abc', ' uvw ', 'uvw xyz']

        it(right[0] + ' is right search query', (done) => {
            expect(val.isQueryValid(right[0])).to.equal(true)
            done()
        })

        const wrong = [ '', 8, '       ']

        wrong.forEach(function(query) {
            it(query + ' is wrong search query', (done) => {
                expect(val.isQueryValid(query)).to.equal(false)
                done()
            })
        })

        it('validatation middleware should pass the query', (done) => {
            expect(validateSearch({ name: right[0] })).to.be.an('undefined')

            done()
        })
    })

    describe('search', () => {
        it('it should return medicine', (done) => {
            chai
                .request(server)
                .post('/medicines/search')
                .set('Cookie', cookie)
                .send({
                    name: 'abc'
                })
                .end(function(err, res) {
                    expect(err).to.be.null
                    expect(res).to.have.status(200)

                    done()
                })
        })

        it('it should not return medicine', (done) => {
            chai
                .request(server)
                .post('/medicines/search')
                .set('Cookie', cookie)
                .send({
                    name: '8989'
                })
                .end(function(err, res) {
                    expect(err).to.be.null
                    expect(res.body).to.be.an('array')
                    expect(res.body.length).to.equal(0)

                    done()
                })
        })
    })
})
