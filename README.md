# medAssist

POST /signup

body {
    email: '',
    name: 'Take care',
    password: '',
    mobNo: 9657681798,
    address: {
        line_1: 'Ganganagar',
        line_2: 'Phursungi',
        country: 'India',
        city: 'Pune',
        state: 'Maharashtra',
        pincode: 412308
    }
}

Note: Verify the registration by clicking on link in mail. Without verification,
signin is not allowed.

POST /signin

body = {
    email: '',
    password: ''
}

Note: Hereon, you need to provide Cookie header. After signin, get its value from
Set-Cookie header.

POST /patients/register

body = {
    name: 'Jane Doe',
    email: '',
    gender: 'F',
    age: 44,
    mobNo: 9657681798,
    address: {
        line_1: 'Ganganagar',
        line_2: 'Phursungi',
        country: 'India',
        city: 'Pune',
        state: 'Maharashtra',
        pincode: 412308
    }
};

POST /patients/view

body = {
    from: 1,
    to: 3
}

DEPLOY ON HEROKU

Heroku
        Create an account on Heroku

        Install heroku CLI

        Follow the instructions at:
        https://devcenter.heroku.com/articles/getting-started-with-nodejs

Update Database
        degrees.json, specialty.json, medicines.json hold documents.

        Use update_db.js to add those docs to database
        $ export DATABASE_URI='your_string'

        $ node update_db.js fillMedicines
        $ node update_db.js fillSpecialty
        $ node update_db.js fillDegSpec

Environment Vars
        Have a look at the .env file. It is hidden.

        Set up those env variables on Heroku.

        Replace any local values such as database strings with the value of production
        string.

Files
        models/
                Database schemas

        server.js
                Main file

        connections.js
                Database connections

        data/
                Database directory. Stores database for local run.

Run locally
        $ export NODE_ENV=dev
        $ npm run dev
