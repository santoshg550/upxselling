const express = require('express')
const bodyParser = require('body-parser')
const session = require('express-session')
const compression = require('compression');
const MongoStore = require('connect-mongo')(session)
const helmet = require('helmet');
const mongoose = require('mongoose')
const responseTime = require('response-time');
const addRequestId = require('express-request-id');

const log = require('./lib/log')
const mail = require('./lib/mail')
const config = require('./config')
const rateLimiter = require('./rate_limiter')

const index = require('./routes/index')
const patients = require('./routes/patients')
const doctors = require('./routes/doctors')
const visits = require('./routes/visits')
const reports = require('./routes/reports')
const medicines = require('./routes/medicines')

const app = express()

app
    .set('view engine', 'ejs')
    .use(compression())
    .use(express.static('public'))
    .use(bodyParser.json({
        limit: '10mb'
    }))
    .use(helmet())
    .use(helmet.contentSecurityPolicy({
        directives: {
            defaultSrc: ["'self'"],
            styleSrc: [
                "'self'",
                'ajax.googleapis.com',
                'maxcdn.bootstrapcdn.com',
                'cdnjs.cloudflare.com'
            ],
            imgSrc: ["'self'"],
            fontSrc: ['cdnjs.cloudflare.com'],
            connectSrc: ["'self'"]
        },
        setAllHeaders: true
    }))
    .use(helmet.noCache())
    .use(session({
        name: 'sessionId',
        secret: config.sessionSecret,
        store: new MongoStore({
            mongooseConnection: mongoose.connection,
            stringify: true
        }),
        saveUninitialized: true,
        resave: false
    }))
    .use(errors.error)
    .disable('x-powered-by')

app
    .use(addRequestId())
    .use(responseTime())
    .use((req, res, next) => {
        log.info({ req: req })

        res.header('X-Request-Id', req.id)

        function afterRes() {
            res.removeListener('finish', afterRes)
            res.removeListener('close', afterRes)

            log.info({ res: res })
        }

        res.on('finish', afterRes)
        res.on('close', afterRes)

        next()
    })
    .use('/signin', rateLimiter())
    .use([ '/patients', '/doctors', '/visitors', '/reports', '/medicines' ], (req, res, next) => {
        if(req.session.uname)
            return next()

        const err = new Error('Unauthorized')
        err.status = 401
        next(err)
    })
    .use('/patients', patients)
    .use('/doctors', doctors)
    .use('/visits', visits)
    .use('/medicines', medicines)
    .use('/reports', reports)
    .use('/', index)
    .use((req, res, next) => {
        const err = new Error('Not Found')
        err.status = 404
        next(err)
    })
    .use((err, req, res, next) => {
        log.error({ err: err }, 'Error handling middleware')

        res.status(err.status || 500).render('error', {
            message: err.message
        })
    })

module.exports = app
