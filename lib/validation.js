const checkType = Object.prototype.toString

const createDOMPurify = require('dompurify')
const { JSDOM } = require('jsdom')

const window = (new JSDOM('')).window
const DOMPurify = createDOMPurify(window)

/*
 * TODO: Array of passwords can be checked. Useful in password reset.
 */
const validPassword = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[`~!@#$%&?;:,.|<>*+\-]).{6,20}/;

function checkString(value) {
    return checkValue(value, 'String')
}

function checkNumber(value) {
    return checkValue(value, 'Number')
}

function checkObject(value) {
    return checkValue(value, 'Object')
}

function checkArray(value) {
    return checkValue(value, 'Array')
}

function checkDate(value) {
    return checkValue(value, 'Date')
}

function checkValue() {
    let length = 0

    let next = 0

    const pairs = arguments

    while(next < pairs.length) {
        let value = pairs[next++]
        let validType = pairs[next++]

        if(checkType.call(value) == '[object ' + validType + ']') {
            switch(validType) {
                case 'Object':
                    length = Object.keys(value).length
                    break

                case 'String':
                    length = value.length
                    break

                case 'Array':
                    length = value.length

                    if(length > this.maxArraylength)
                        return false

                    break

                case 'Number':
                    continue
            }

            if(length == 0)
                return false
        } else if(validType == 'Date' && new Date(value) != 'Invalid Date')
            return true
        else
            return false

        if([ 'String', 'Number' ].includes(validType) && !DOMPurify.sanitize(value))
            return false
    }

    return true
}

module.exports = {
    validPassword: validPassword,
    checkValue: checkValue,
    checkString: checkString,
    checkNumber: checkNumber,
    checkObject: checkObject,
    checkArray: checkArray,
    checkDate: checkDate
}
