const nodemailer = require('nodemailer')

const config = require('../config')
const log = require('./log')

module.exports = function(sendTo, sub, body, callback) {
    const transporter = nodemailer.createTransport({
        service: 'Gmail',
        auth: {
            user: config.email.id,
            pass: config.email.password
        }
    })

    const mailOptions = {
        from: config.email.id,
        to: sendTo,
        subject: sub,
        text: body.text,
        html: body.html
    }


    transporter.sendMail(mailOptions, (err, info) => {
        if(err)
            log.error({ err: err }, 'Unable to send mail')

        callback(err)
    })
}
