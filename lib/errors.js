const { checkValue } = require('./validation')
const log = require('./log')

const sww = 'Something went wrong'

function dbError(err) {
    log.error({ err: err }, 'Database error');

    return {
        code: err.code,
        message: sww
    }
}

/*
 * Method to use for synchronous errors.
 */
function buildError(err) {
    if(checkValue(err, 'String') || !err)
        return {
            error: true,
            message: err || sww
        }

    return Object.assign({ error: true }, err)
}

function error(req, res, next) {
    res.error = (status, err) => {
        if(err && !err.error)
            err = buildError(err)

        res.status(status).json(err ? err : buildError(sww))
    }

    next()
}

module.exports = {
    error: error,

    buildError: buildError,

    dbError: dbError,

    NameError: buildError('Name is wrong'),

    EmailError: buildError('Email is wrong'),

    MobileError: buildError('Mobile number is wrong'),

    AddressError: buildError('Address is wrong')
}
