const express = require('express')

const handler = require('./visits_handler.js')

const router = express.Router()

router.use('/add', (req, res, next) => {
    const error = handler.validateVisit(req, res, next)

    if(error)
        return res.error(400, error)
})

router.post('/add', handler.add)

router.use('/update', (req, res, next) => {
    const error = handler.validateUpdate(req, res, next)

    if(error)
        return res.error(400, error)
})

router.post('/update', handler.updateVisit)

router.use('/', (req, res, next) => {
    const error = handler.validateView(req.body)

    if(error)
        return res.error(400, error)

    next()
})

router.post('/', handler.view)

module.exports = router
