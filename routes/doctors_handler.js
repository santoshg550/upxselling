const mongoose = require('mongoose')

const util = require('../lib/util')
const { Doctor, Specialty, Degree } = require('../models/doctor')
const log = require('../lib/log')
const val = require('./validations').doctor

function validateReg(doctor) {
    if(!val.isEmailValid(doctor.email))
        return errors.EmailError

    if(!val.isNameValid(doctor.name))
        return errors.NameError

    if(!val.isMobValid(doctor.mobNo))
        return errors.MobileError

    if(!val.isAddressValid(doctor.address))
        return errors.AddressError

    let degErr = val.isDegValid(doctor.degrees)
    if(degErr)
        return degErr

    let specErr = val.isSpecValid(doctor.specialties)
    if(specErr)
        return specErr
}

function register(req, res) {
    const reqBody = req.body

    Doctor.findOne({
        clinicEmail: req.session.uname,
        email: reqBody.email
    }, (err, doctor) => {
        if(err)
            return res.error(500, errors.dbError(err).message)

        if(doctor)
            return res.error(409, 'Email already exists')

        Doctor.findOne({
            clinicEmail: req.session.uname,
            mobNo: reqBody.mobNo
        }, (err, doctor) => {
            if(err)
                return res.error(500, errors.dbError(err).message)

            if(doctor)
                return res.error(409, 'Mobile number already exists')

            doctor = new Doctor(reqBody)

            if(doctor.degrees)
                doctor.degrees = util.removeDup(doctor.degrees, 'degree')

            if(doctor.specialties)
                doctor.specialties = util.removeDup(doctor.specialties, 'specialty')

            doctor.clinicEmail = req.session.uname

            doctor.save((err) => {
                if(err)
                    return res.error(500, errors.dbError(err).message)

                res.end('Record added')
            })
        })
    })
}

function record(req, res) {
    Doctor.find({ clinicEmail: req.session.uname }, (err, doctors) => {
        if(err)
            return res.error(500, errors.dbError(err).message)

        if(!doctors.length)
            return res.error(404, 'No such clinic')

        res.json(doctors)
    })
}

function updateReg(req, res) {
    const reqBody = req.body

    Doctor.findOne({ _id: reqBody._id }, (err, doctor) => {
        if(err)
            return res.error(500, errors.dbError(err).message)

        if(!doctor)
            return res.error(400, 'No such doctor')

        doctor.email = reqBody.email
        doctor.name = reqBody.name
        doctor.mobNo = reqBody.mobNo
        doctor.address = reqBody.address
        doctor.specialties = reqBody.specialties
        doctor.degrees = reqBody.degrees

        if(doctor.degrees)
            doctor.degrees = util.removeDup(doctor.degrees, 'degree')

        if(doctor.specialties)
            doctor.specialties = util.removeDup(doctor.specialties, 'specialty')

        doctor.save((err) => {
            if(err)
                return res.error(500, errors.dbError(err).message)

            res.end('Record updated')
        })
    })
}

function specialties(req, res) {
    Specialty.find((err, specialties) => {
        if(err)
            return res.error(500, errors.dbError(err).message)

        res.json(specialties)
    })
}

function degrees(req, res) {
    Degree.find((err, degrees) => {
        if(err)
            return res.error(500, errors.dbError(err).message)

        res.json(degrees)
    })
}

module.exports = {
    validateReg: validateReg,
    register: register,
    updateReg: updateReg,
    validateUpdate: validateReg,
    record: record,
    degrees: degrees,
    specialties: specialties
}
