const crypto = require('crypto')
const url = require('url')
const fs = require('fs')
const ejs = require('ejs')

const Clinic = require('../models/clinic').Clinic
const Token = require('../models/clinic').Token
const crypt = require('../lib/crypt')
const genRandomString = require('../lib/util').genRandomString
const mail = require('../lib/mail')
const config = require('../config')
const val = require('./validations').clinic

const emailTemp = fs.readFileSync('./views/signup_email.html').toString()

function sendVerificationToken(req, res, user) {
    genRandomString(20, 'hex', (err, buf) => {
        if(err)
            return res.error(500);

        const token = new Token({
            userId: user.email,
            token: buf.toString('hex')
        })

        token.save((err) => {
            if(err)
                return res.error(500, errors.dbError(err).message + '\nPlease try again later.');

            const sendTo = user.email

            const sub = 'Medassist - verify email'

            const body = {
                html: ejs.render(emailTemp, {
                    clinic: {
                        name: user.name,
                        verLink: url.format({
                            protocol: req.protocol,
                            pathname: 'verify',
                            host: req.headers.host,
                            query: {
                                email: sendTo,
                                token: token.token
                            }
                        })
                    }
                })
            }

            mail(sendTo, sub, body, (err) => {
                if(err)
                    return res.error(500, 'Failed to mail the verification link to you. Please try again later.');

                res.end('A verification email has been sent to ' + user.email + '.');
            })
        })
    })
}

function register(req, res, next) {
    const reqBody = req.body

    Clinic.findOne({ email: reqBody.email }, (err, user) => {
        if(err)
            return res.error(500, errors.dbError(err).message)

        if(user)
            return res.error(400, 'Username already exists')

        Clinic.findOne({ mobNo: reqBody.mobNo }, (err, user) => {
            if(err)
                return res.error(500, errors.dbError(err).message)

            if(user)
                return res.error(400, 'Mobile number already exists')

            const credParams = {
                iterations: config.iterations,
                keyLength: config.keyLength,
                digest: config.digest,
                saltLength: config.saltLength,
                byteToStringEncoding: config.byteToStringEncoding
            }

            crypt.hash(reqBody.email, reqBody.password, credParams, null, (err, hashedPsw, salt) => {
                if(err)
                    return res.error(500)

                const newUser = new Clinic({
                    email: reqBody.email,
                    name: reqBody.name,
                    password: hashedPsw.toString(credParams.byteToStringEncoding),
                    mobNo: reqBody.mobNo,
                    address: reqBody.address,
                    logo: reqBody.logo ? Buffer(reqBody.logo, 'base64') : undefined,
                    salt: salt,
                    isVerified: false,
                })

                newUser.save((err) => {
                    if(err)
                        return res.error(500, errors.dbError(err).message)

                    sendVerificationToken(req, res, newUser)
                })
            })
        })
    })
}

function verify(req, res, next) {
    Token.findOne({ userId: req.query.email, token: req.query.token }, (err, token) => {
        if(err)
            return res.error(500, errors.dbError(err).message)

        if(!token) {
            req.flag = {
                msg: 'Asking to verify with wrong token!'
            }

            return res.status(403).end()
        }

        Clinic.updateOne(
            {
                email: req.query.email
            }, {
                $set: {
                    isVerified: true
                }
            }, (err) => {
                if(err)
                    return res.error(500, errors.dbError(err).message + '\nPlease verify again.')

                Token.deleteOne({ userId: req.query.email }, (err) => {
                    if(err)
                        errors.dbError(err)
                })

                res.render('welcome', {
                    message: 'Email verified. Thank you.'
                })
            })
    })
}

function record(req, res) {
    Clinic.findOne({ email: req.session.uname }, (err, clinic) => {
        if(err)
            return res.error(500, errors.dbError(err).message)

        if(!clinic)
            return res.error(400, 'No such client')

        clinic.password = undefined
        clinic.salt = undefined

        res.json(clinic)
    })
}

function updateReg(req, res) {
    const reqBody = req.body

    function update(err, clinic) {
        if(err)
            return res.error(500, errors.dbError(err).message)

        if(clinic.email !== reqBody.email)
            clinic.isVerified = false

        clinic.email = reqBody.email

        clinic.name = reqBody.name

        clinic.mobNo = reqBody.mobNo

        clinic.address = reqBody.address

        clinic.save(err, (err) => {
            if(err)
                return res.error(500, errors.dbError(err).message)

            req.session.uname = reqBody.email

            if(!clinic.isVerified)
                return sendVerificationToken(req, res, clinic)

            res.end('Updated')
        })
    }

    if(reqBody.email && reqBody.email == req.session.uname)
        return Clinic.findOne({ email: req.session.uname }, update)

    Clinic.findOne({ email: reqBody.email }, (err, user) => {
        if(err)
            return res.error(500, errors.dbError(err).message)

        if(user)
            return res.error(400, 'That email has been registered for another clinic')

        Clinic.findOne({ email: req.session.uname }, update)
    })
}

function resend(req, res, next) {
    Clinic.findOne({ email: req.body.email }, (err, user) => {
        if(err)
            return res.error(500, errors.dbError(err).message)

        if(!user) {
            req.flag = {
                msg: 'Asking to verify even if not a user!'
            }

            return res.status(403).end()
        }

        if(user.isVerified)
            return res.end('You already verified your account.')

        sendVerificationToken(req, res, user)
    })
}

module.exports = {
    register: register,
    verify: verify,
    resend: resend,
    updateReg: updateReg,
    record: record
}
