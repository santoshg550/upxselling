const crypto = require('crypto')
const url = require('url')
const fs = require('fs')
const ejs = require('ejs')

const User = require('../models/clinic').Clinic
const crypt = require('../lib/crypt')
const genRandomString = require('../lib/util').genRandomString
const mail = require('../lib/mail')
const config = require('../config')

const forgotTemp = fs.readFileSync('./views/partials/forgot_password.ejs').toString()
const resetTemp = fs.readFileSync('./views/partials/reset_password.ejs').toString()

const credParams = {
    iterations: config.iterations,
    keyLength: config.keyLength,
    digest: config.digest,
    saltLength: config.saltLength,
    byteToStringEncoding: config.byteToStringEncoding
}

function forgot(req, res, next) {
    User.findOne({ email: req.body.email }, (err, user) => {
        if(err)
            return res.error(500, errors.dbError(err).message)

        if(!user)
            return res.error('User does not exist')

        genRandomString(config.resetTokenLength, 'hex', (err, buf) => {
            if(err)
                return res.error(500)

            var token = buf.toString(config.byteToStringEncoding)

            crypt.hash(user.email, token, credParams, null, (err, hashedToken, salt) => {
                if(err)
                    return res.error(500)

                user.resetPasswordToken = hashedToken.toString(config.byteToStringEncoding)
                user.resetPasswordSalt = salt
                user.resetPasswordExpires = Date.now() + config.resetTokenExpires

                user.save((err) => {
                    if(err)
                        return res.error(500, errors.dbError(err).message)

                    var resetURL = url.format({
                        protocol: req.protocol,
                        host: req.headers.host,
                        pathname: 'reset',
                        query: {
                            email: user.email,
                            token: token
                        }
                    })

                    var sub = 'Medassist - Password Reset'

                    const body = {
                        html: ejs.render(forgotTemp, {
                            resetURL: resetURL
                        })
                    }

                    mail(user.email, sub, body, (err) => {
                        if(err)
                            return res.error(500, 'Failed to mail to you.')

                        res.end('An email has been sent to ' + user.email)
                    })
                })
            })
        })
    })
}

function verifyReq(req, res, callback) {
    var query = req.query

    User.findOne({
        email: query.email
    }, (err, user) => {
        if(err)
            return res.error(500, errors.dbError(err).message)

        if(!user)
            return res.error(401, 'User does not exist')

        if(Date.now() > user.resetPasswordExpires)
            return res.error(401, 'Reset token expired')

        crypt.hash(query.email, query.token, credParams, user.resetPasswordSalt, (err, hashedToken) => {
            if(err)
                return res.error(500)

            if(!crypto.timingSafeEqual(Buffer.from(user.resetPasswordToken, credParams.byteToStringEncoding), hashedToken)) {
                req.flag = {
                    msg:  'Invalid reset token',
                    userId: user.email,
                    token: query.token
                }

                return res.error(401, req.flag.msg)
            }

            callback(user)
        })
    })
}

function form(req, res, next) {
    verifyReq(req, res, () => {
        res.render('reset', { title: 'Medassist - Password Reset' })
    })
}

function resetPassword(req, res, next) {
    verifyReq(req, res, (user) => {
        crypt.hash(user.email, req.body.password, credParams, null, (err, hashedPsw, salt) => {
            if(err)
                return res.error(500)

            user.password = hashedPsw.toString(credParams.byteToStringEncoding)
            user.salt = salt

            user.resetPasswordToken = ''
            user.resetPasswordExpires = ''
            user.resetPasswordSalt = ''

            user.save((err) => {
                if(err)
                    return res.error(500, errors.dbError(err).message)

                var sendTo = user.email

                var sub = 'Your password has been changed'

                const body = {
                    html: ejs.render(resetTemp, {
                        sendTo: sendTo
                    })
                }

                mail(sendTo, sub, body, (err) => {
                    if(err)
                        return res.error(500, 'Password changed successfully but failed to mail to you')

                    res.end('Password changed successfully')
                })
            })
        })
    })
}

module.exports = {
    form: form,
    resetPassword: resetPassword,
    forgot: forgot
}
