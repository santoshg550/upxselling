const express = require('express')

const handler = require('./reports_handler.js')

const router = express.Router()

router.use('/income', (req, res, next) => {
    const error = handler.validateRange(req.body)

    if(error)
        return res.error(400, error)

    next()
})

router.post('/income', handler.income)

router.use('/visits', (req, res, next) => {
    const error = handler.validateRange(req.body)

    if(error)
        return res.error(400, error)

    next()
})

router.post('/visits', handler.visitors)

module.exports = router
