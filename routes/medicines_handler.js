const mongoose = require('mongoose')

const Visit = require('../models/visit')
const Medicine = require('../models/medicine')
const log = require('../lib/log')
const val = require('./validations').medicine

function validateSearch(query) {
    if(!val.isQueryValid(query.name))
        return buildError('Search param is wrong')
}

function search(req, res) {
    Medicine.find({
        name: {
            $regex: req.body.name,
            $options: 'i'
        }
    }, (err, medicines) => {
        if(err)
            return res.error(500, errors.dbError(err).message)

        res.json(medicines)
    })
}

module.exports = {
    validateSearch: validateSearch,
    search: search
}
