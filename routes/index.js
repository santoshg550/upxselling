const express = require('express')

const signin = require('./signin')
const signup = require('./signup')
const reset = require('./reset')
const { clinic } = require('./validations')
const { checkString } = require('../lib/validation')

const router = express.Router()

router.get('/signin', (req, res, next) => {
    res.render('signin')
})

router.get('/signup', (req, res, next) => {
    res.render('signup')
})

router.get('/resend', (req, res, next) => {
    res.render('resend')
})

router.use([ '/signin', '/signup', '/resend', '/client/update' ], (req, res, next) => {
    if(!clinic.isEmailValid(req.body.email))
        return res.status(400).end('Email is wrong')

    next()
})

router.use([ '/signin', '/signup' ], (req, res, next) => {
    if(!clinic.isPasswordValid(req.body.password))
        return res.status(400).end('Password is wrong')

    next()
})

router.post('/signin', signin)

router.use([ '/signup', '/client/update' ], (req, res, next) => {
    if(!clinic.isNameValid(req.body.name))
        return res.status(400).end('Name is wrong')

    if(!clinic.isMobValid(req.body.mobNo))
        return res.status(400).end('Mobile number is wrong')

    if(!clinic.isAddressValid(req.body.address))
        return res.status(400).end('Address is wrong')

    if(req.body.logo && !clinic.isLogoValid(req.body.logo))
        return res.status(400).end('Logo is wrong')

    next()
})

router.post('/signup', signup.register)

router.use('/verify', (req, res, next) => {
    if(!clinic.isEmailValid(req.query.email))
        return res.status(400).end('Email is wrong')

    if(!checkString(req.query.token))
        return res.status(400).end('Token is wrong')

    next()
})

router.get('/verify', signup.verify)

router.post('/resend', signup.resend)

router.post('/forgot', reset.forgot)

router.get('/reset', reset.form)

router.post('/reset', function(req, res, next) {
    if(clinic.isPasswordValid(req.body.password) && clinic.isPasswordValid(req.body.confirm))
        if(req.body.password == req.body.confirm)
            return next()

    req.flag = {
        msg: 'Invalid password'
    }

    res.end()
}, reset.resetPassword)

router.use((req, res, next) => {
    if(req.session.uname)
        return next()

    res.render('welcome', { title : 'Med Assist - Upxselling' })
})

router.get('/client', signup.record)

router.post('/client/update', signup.updateReg)

router.get('/', (req, res, next) => {
    res.render('home', {
        title: 'Welcome to Med Assist - Upxselling'
    })
})

router.get('/signout', (req, res, next) => {
    req.session.destroy((err) => {
        if(err) {
            req.flag = {
                msg: 'Session can not be destroyed.'
            }

            return res.error(500, req.flag.msg)
        }

        res.render('welcome', { title : 'Med Assist - Upxselling' })
    })
})

module.exports = router
