const crypto = require('crypto')
const url = require('url')

const Clinic = require('../models/clinic').Clinic
const crypt = require('../lib/crypt')
const genRandomString = require('../lib/util').genRandomString
const mail = require('../lib/mail')
const config = require('../config')

const emailTemp = fs.readFileSync('./views/partials/forgot_password.html').toString()

module.exports = function(req, res, next) {
    User.findOne({ email: req.body.email }, (err, user) => {
        if(err)
            return res.error(errors.dbError(err).message)

        if(!user)
            return res.error('User does not exist.')

        genRandomString(config.resetTokenLength, 'hex', (err, buf) => {
            if(err)
                return res.error(500)

            var token = buf.toString(config.byteToStringEncoding)

            const credParams = {
                iterations: config.iterations,
                keyLength: config.keyLength,
                digest: config.digest,
                saltLength: config.saltLength,
                byteToStringEncoding: config.byteToStringEncoding
            }

            crypt.hash(user.email, token, credParams, null, (err, hashedToken, salt) => {
                if(err)
                    return res.error(500)

                user.resetPasswordToken = hashedToken.toString(config.byteToStringEncoding)
                user.resetPasswordSalt = salt
                user.resetPasswordExpires = Date.now() + config.resetTokenExpires

                user.save((err) => {
                    if(err)
                        return res.error(errors.dbError(err).message)

                    var resetURL = url.format({
                        protocol: req.protocol,
                        host: req.headers.host,
                        pathname: 'reset',
                        query: {
                            email: user.email,
                            token: token
                        }
                    })

                    var sub = 'Monek - Password Reset'

                    const body = {
                        html: ejs.render(emailTemp, {
                            resetURL: resetURL
                        })
                    }

                    mail(user.email, sub, body, (err) => {
                        if(err)
                            return res.error('Failed to mail to you.')

                        res.end('An email has been sent to ' + user.email)
                    })
                })
            })
        })
    })
}
