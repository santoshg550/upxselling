const express = require('express')

const doctor = require('./validations').doctor
const handler = require('./doctors_handler.js')

const router = express.Router()

router.get('/', handler.record)

router.get('/degrees', handler.degrees)

router.get('/specialties', handler.specialties)

router.use('/register', (req, res, next) => {
    const error = handler.validateReg(req.body)

    if(error)
        return res.error(400, error)

    next()
})

router.post('/register', handler.register)

router.use('/update', (req, res, next) => {
    const error = handler.validateUpdate(req.body)

    if(error)
        return res.error(400, error)

    next()
})

router.post('/update', handler.updateReg)

module.exports = router
