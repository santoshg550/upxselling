const mongoose = require('mongoose')

const Patient = require('../models/patient')
const Counter = require('../models/counter')
const log = require('../lib/log')
const val = require('./validations').patient

function validateReg(patient) {
    if(!val.isFNValid(patient.firstName))
        return buildError('First name is wrong')

    if(!val.isLNValid(patient.lastName))
        return buildError('Last name is wrong')

    if(!val.isEmailValid(patient.email))
        return buildError('Email is wrong')

    if(!val.isGenderValid(patient.gender))
        return buildError('Gender is wrong')

    if(!val.isAgeValid(patient.age))
        return buildError('Age is wrong')

    if(!val.isMobValid(patient.mobNo))
        return buildError('Mobile number is wrong')

    if(!val.isAddressValid(patient.address))
        return buildError('Address is wrong')
}

function register(req, res, next) {
    function regPatient(seq) {
        const reqBody = req.body

        const patient = new Patient({
            id: seq,
            clinicId: req.session._id,
            firstName: reqBody.firstName,
            lastName: reqBody.lastName,
            gender: reqBody.gender,
            age: reqBody.age,
            mobNo: reqBody.mobNo,
            email: reqBody.email,
            address: reqBody.address
        })

        patient.save((err) => {
            if(err)
                return res.error(500, errors.dbError(err).message)

            res.end('Patient added')
        })
    }

    Counter.getCount(req.session._id, (err, doc) => {
        if(err)
            return log.error({ err: err }, 'Error getting autoincrement id for patient')

        if(!doc) {
            const counter = new Counter({
                clinicId: req.session._id,
                count: 1
            })

            return counter.save((err) => {
                if(err)
                    return log.error({ err: err }, 'Error creating clinic counter for patients')

                regPatient(counter.count)
            })
        }

        regPatient(doc.count)
    })
}

function validateUpdate(update) {
    if(!val.isIdValid(update.id))
        return buildError('Id is wrong')

    return validateReg(update)
}

function updateReg(req, res, next) {
    const update = req.body

    Patient.findOne({ id: req.body.id }, (err, patient) => {
        if(err)
            return res.error(500, errors.dbError(err).message)

        if(!patient)
            return res.error(400, 'No such patient')

        patient.email = update.email

        patient.gender = update.gender

        patient.age = update.age

        patient.firstName = update.firstName

        patient.lastName = update.lastName

        patient.mobNo = update.mobNo

        patient.address = update.address

        patient.save((err) => {
            if(err)
                return res.error(500, errors.dbError(err).message)

            res.end('Updated')
        })
    })
}

function validateView(view) {
    if(view.id && !val.isIdValid(view.id))
        return buildError('Wrong id')

    if(view.from)
        if(val.isFromToValid(view.from, view.to)) {
            return
        } else
            return buildError('Wrong (from, to)')
}

function view(req, res) {
    const reqBody = req.body

    if(reqBody.id)
        Patient.find({
            id: reqBody.id
        }, onPatients)
    else if(reqBody.from)
        Patient.find({
            id: {
                $gte: reqBody.from,
                $lte: reqBody.to
            },
            clinicId: req.session._id
        }, onPatients)
    else
        Patient.find({ clinicId: req.session._id }, onPatients)

    function onPatients(err, patients) {
        if(err)
            return res.error(500, errors.dbError(err).message);

        if(!patients.length)
            return res.error(404, 'No such patients')

        res.json(patients)
    }
}

function validateSearch(query) {
    if(!val.isQueryValid(query.firstName || query.lastName))
        return buildError('Search by first or last name with minimum of 3 characters.')
}

function search(req, res) {
    const reqBody = req.body

    if(reqBody.firstName)
        var query = {
            firstName: {
                $regex: reqBody.firstName,
                $options: 'i'
            }
        }
    else
        var query = {
            lastName: {
                $regex: reqBody.lastName,
                $options: 'i'
            }
        }

    Patient.find(query, (err, patients) => {
        if(err)
            return res.error(500, errors.dbError(err).message);

        if(!patients.length)
            return res.error(404, 'No such patients')

        const resBody = []
        for(let i = 0; i < patients.length; i++)
            resBody.push({
                id: patients[i].id,
                firstName: patients[i].firstName,
                lastName: patients[i].lastName
            })

        res.json(resBody)
    })
}

module.exports = {
    validateReg: validateReg,
    register: register,
    validateView: validateView,
    view: view,
    validateUpdate: validateReg,
    updateReg: updateReg,
    validateSearch: validateSearch,
    search: search
}
