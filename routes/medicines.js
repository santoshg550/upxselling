const express = require('express')

const handler = require('./medicines_handler.js')

const router = express.Router()

router.use('/search', (req, res, next) => {
    const error = handler.validateSearch(req.body)

    if(error)
        return res.error(400, error)

    next()
})

router.post('/search', handler.search)

module.exports = router
