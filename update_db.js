const mongoose = require('mongoose')
const fs = require('fs')

const { Degree, Specialty } = require('./models/doctor')
const Medicine = require('./models/medicine')
const Visit = require('./models/visit')

require('dotenv/config')

mongoose.connect(process.env.DATABASE_URI)

mongoose.connection.on('open', () => {
    if(!process.argv[2])
        return console.log('Which function do you want to execute ?')

    queries[process.argv[2]]()
})

/*
 * Mongoose places null for keys with values undefined
 */
const queries = {
    fillMedicines: () => {
        mongoose.connection.db.listCollections({
            name: 'medicines'
        }).next((err, coll) => {
            if(err)
                throw err

            if(coll)
                return

            fs.readFile('medicines.json', (err, data) => {
                const medicines = JSON.parse(data) 

                Medicine.create(medicines, (err) => {
                    if(err)
                        throw err
                })
            })
        })
    },

    fillDegSpec: () => {
        if(process.argv[2] == 'specialties' || process.argv[2] == 'all')
            mongoose.connection.db.listCollections({
                name: 'specialties'
            }).next((err, coll) => {
                if(err)
                    throw err

                if(coll)
                    return

                fs.readFile('specialty.json', (err, data) => {
                    const specialty = JSON.parse(data) 

                    Specialty.create(specialty, (err) => {
                        if(err)
                            throw err
                    })
                })
            })

        if(process.argv[2] == 'degrees' || process.argv[2] == 'all')
            mongoose.connection.db.listCollections({
                name: 'degrees'
            }).next((err, coll) => {
                if(err)
                    throw err

                if(coll)
                    return

                fs.readFile('degrees.json', (err, data) => {
                    const degrees = JSON.parse(data) 

                    Degree.create(degrees, (err) => {
                        if(err)
                            throw err
                    })
                })
            })
    },

    fillSpecialty: () => {
        clinic.Specialty.findOne({ specialty: 'abc' }, (err, specialty) => {
            if(err)
                return console.log(err)

            // Somehow $addToSet adds duplicate
            clinic.Clinic.findOneAndUpdate({
                email: 'santoshg550@gmail.com',
                'staff.email': 'uvw@gmail.com'
            }, {
                $addToSet: {
                    'staff.$.specialties': {
                        experience: 8,
                        residency: 'abc',
                        authority: 'alal',
                        specialty: specialty
                    }
                }
            }, (err) => {
                if(err)
                    console.log(err)
            })
        })
    },

    /*
     * Query for docs with at least one entry in array
     */
    arrayLength: () => {
        const query = [
            { 'medicines.0': { $exists: 1 }},

            /*
             * Javascript executes more slowly than the native operators
             */
            { $where: "this.medicines.length > 0" }
        ]

        Visit.find(query[1], (err, docs) => {
            if(err)
                console.log(err)

            console.log(docs)
        })
    }
}
