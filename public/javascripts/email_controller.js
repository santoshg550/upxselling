var app = angular.module('upxselling', [])

app.controller('upxCtrl', function($scope, $http, $compile) {
    $scope.showAddPatient = true

    $scope.checkRemPatient = false

    $scope.showAddPanel = function() {
        $scope.showAddPatient = false
    }

    $scope.closeAddPanel = function() {
        $scope.showAddPatient = true
    }

    $scope.addPatient = function() {
        $http.post('add', {
            name: $scope.name,
            email: $scope.email,
            age: $scope.age,
        }).then(function success(res) {
            var newElm = angular.element(res.data)
            $compile(newElm)($scope)

            $('.panel-group').append(newElm)
        }, function failure(res) {
            alert(res.data)
        })
    }
})
